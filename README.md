**Cuarto proyecto de la carrera Desarrollo Web Full Stack. El objetivo del proyecto está en la introducción a jQuery. El html, CSS, algunos JS y archivos de dibujos con JSON fueron brindados por el instituto. La lógica desarrollada fue todo lo referente a JQuery.**

---

## Objetivo del proyecto
Este proyecto busca crear una herramienta para hacer diferentes dibujos pintando pixel por pixel. Una especia de lienzo para pintar y dibujar con pequeños cuadrados conocidos como píxeles. La aplicación funciona online, se ejecuta en un navegador, y tiene funcionalidades que te permiten editar algunas imágenes pre-existentes o crear las tuyas.

El foco principal del proyecto está en la introducción a jQuery, una biblioteca de JavaScript que quienes hacen desarrollo web utilizan con frecuencia para hacerles más fácil la vida. Con jQuery vas a aprender a agregar funcionalidades a cualquier página que desarrolles usando eventos y manipulando elementos. Esto mismo vas a hacer en el proyecto, además de incluir efectos de animación.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-PixelArt.
Se puede ejecutar el mismo abriendo el archivo index.html.

---

## Descripción de como se llevo a cabo el proyecto
* Primero analice todos los componentes que ya venian solucionados. HTML, CSS y algunos JS.
* Luego cargue la grilla de pixeles donde se va a dibujar y agregue la paleta de colores para que se pueda seleccionar el color para pintar.
* Luego fui agregando las funcionalidades con el evento click: Cambiar color, Pintar pixel en la grilla, Detectar cuando se mantiene presionado el click para que se pinte de manera continua, funcionalidad para guardar el dibujo y dibujar otros dibujos precargados con JSON.

---