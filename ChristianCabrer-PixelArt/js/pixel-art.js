var nombreColores = ['White', 'LightYellow',
    'LemonChiffon', 'LightGoldenrodYellow', 'PapayaWhip', 'Moccasin', 'PeachPuff', 'PaleGoldenrod', 'Bisque', 'NavajoWhite', 'Wheat', 'BurlyWood', 'Tan',
    'Khaki', 'Yellow', 'Gold', 'Orange', 'DarkOrange', 'OrangeRed', 'Tomato', 'Coral', 'DarkSalmon', 'LightSalmon', 'LightCoral', 'Salmon', 'PaleVioletRed',
    'Pink', 'LightPink', 'HotPink', 'DeepPink', 'MediumVioletRed', 'Crimson', 'Red', 'FireBrick', 'DarkRed', 'Maroon',
    'Brown', 'Sienna', 'SaddleBrown', 'IndianRed', 'RosyBrown',
    'SandyBrown', 'Goldenrod', 'DarkGoldenrod', 'Peru',
    'Chocolate', 'DarkKhaki', 'DarkSeaGreen', 'MediumAquaMarine',
    'MediumSeaGreen', 'SeaGreen', 'ForestGreen', 'Green', 'DarkGreen', 'OliveDrab', 'Olive', 'DarkOliveGreen', 'YellowGreen', 'LawnGreen',
    'Chartreuse', 'GreenYellow', 'Lime', 'SpringGreen', 'LimeGreen',
    'LightGreen', 'PaleGreen', 'PaleTurquoise',
    'AquaMarine', 'Cyan', 'Turquoise', 'MediumTurquoise', 'DarkTurquoise', 'DeepSkyBlue',
    'LightSeaGreen', 'CadetBlue', 'DarkCyan', 'Teal', 'Steelblue', 'LightSteelBlue', 'Honeydew', 'LightCyan',
    'PowderBlue', 'LightBlue', 'SkyBlue', 'LightSkyBlue',
    'DodgerBlue', 'CornflowerBlue', 'RoyalBlue', 'SlateBlue',
    'MediumSlateBlue', 'DarkSlateBlue', 'Indigo', 'Purple', 'DarkMagenta', 'Blue',
    'MediumBlue', 'DarkBlue', 'Navy', 'Thistle',
    'Plum', 'Violet', 'Orchid', 'DarkOrchid', 'Fuchsia', 'Magenta', 'MediumOrchid',
    'BlueViolet', 'DarkViolet', 'DarkOrchid',
    'MediumPurple', 'Lavender', 'Gainsboro', 'LightGray', 'Silver', 'DarkGray', 'Gray',
    'DimGray', 'LightSlateGray', 'DarkSlateGray', 'Black'
];

// Variable para guardar el elemento 'color-personalizado'
// Es decir, el que se elige con la rueda de color.
var colorPersonalizado = document.getElementById('color-personalizado');
var IndicadorDeColor = document.getElementById('indicador-de-color');
var mouseclickeado = false;

$(document).ready(function() {
    //Llamo a las funciones creadas para inicializar la aplicacion
    CargarPaletaDeColores();
    CrearGrilla();

    //Agrego los eventos necesarios a los elementos ya creados
    //Funcion que se acciona cuando se elige un color de la paleta
    $('#paleta .color-paleta').on('click', function() {
        CambiarIndicadorDeColor($(this).css('background-color'));
    });

    //Funcion que se acciona cuando se comienza a hacer click con el boton izquierdo del mouse
    $('#grilla-pixeles div').on('mousedown', function() {
        mouseclickeado = true;
        PintarPixel(this);
    });

    //Funcion que se acciona cuando se suelta el boton izquierdo del mouse
    $('#grilla-pixeles div').on('mouseup', function() {
        mouseclickeado = false;
    });

    //Funcion que se acciona cuando se sale de la grilla
    $('#grilla-pixeles').on('mouseleave', function() {
        mouseclickeado = false;
    });

    //Funcion que se acciona cuando se mueve con el mouse por la grilla
    $('#grilla-pixeles div').on('mouseover', function() {
        if (mouseclickeado) {
            PintarPixel(this);
        };
    });

    //Funcion que se acciona cuando se presiona el boton "Borrar Todo"
    $('#borrar').on('click', BorrarTodo);

    //Funcion que se acciona cuando se presiona el boton "Guardar"
    $('#guardar').on('click', guardarPixelArt);

    //Funcion que se acciona cuando se presiona el boton "Pintar Todo"
    $('#pintar_todo').on('click', PintarTodo);

    //Funciones que se acciona cuando se presiona alguno de los superheroes
    $('#batman').on('click', function() {
        cargarSuperheroe(batman);
    });
    $('#wonder').on('click', function() {
        cargarSuperheroe(wonder);
    });
    $('#flash').on('click', function() {
        cargarSuperheroe(flash);
    });
    $('#invisible').on('click', function() {
        cargarSuperheroe(invisible);
    });
});

colorPersonalizado.addEventListener('change',
    (function() {
        // Se guarda el color de la rueda en colorActual
        colorActual = colorPersonalizado.value;
        // Completar para que cambie el indicador-de-color al colorActual
        CambiarIndicadorDeColor(colorActual);
    })
);

//Funcion utilizada para cargar la paleta de colores
function CargarPaletaDeColores() {
    //Obtengo la paleta del DOM
    var paleta = $('#paleta');
    //Recorro los colores para ir agregandolos a la paleta
    for (i = 0; i < nombreColores.length; i++) {
        paleta.append('<div class="color-paleta" style="background-color: ' + nombreColores[i] + '"' + '></div>');
    }
}

//Funcion utilizada para crear la grilla
function CrearGrilla() {
    //Obtengo la grilla del DOM
    var grillapixeles = $('#grilla-pixeles');
    //Recorro 1750 veces el for para cargar cada uno de los "pixeles"(div).
    for (i = 0; i < 1750; i++) {
        grillapixeles.append('<div></div>');
    }
}

//Funcion que cambia el indicador de color seleccionado
function CambiarIndicadorDeColor(Color) {
    IndicadorDeColor.style.backgroundColor = Color;
}

//Funcion que se utiliza para pintar el pixel clickeado en la grilla.
function PintarPixel(pixel) {
    $(pixel).css('background-color', IndicadorDeColor.style.backgroundColor);
}

//Funcion que se utiliza para borrar toda la pantalla con una animación.
function BorrarTodo() {
    $("#grilla-pixeles div").animate({ "background-color": "#ffffff" }, 2000);
}

//Funcion que se utiliza para pintar toda la pantalla del color seleccionado.
function PintarTodo(){
    $("#grilla-pixeles div").animate({ "background-color":  IndicadorDeColor.style.backgroundColor}, 2000);
}